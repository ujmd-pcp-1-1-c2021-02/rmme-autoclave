﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autoclave
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnabrir_Click(object sender, EventArgs e)
        {
            pnlpuerta.Height = pnlpuerta.Height + 5;
            pnlpuerta.Top = pnlpuerta.Top - 5;
        }

        private void btncerrar_Click(object sender, EventArgs e)
        {
            pnlpuerta.Height = pnlpuerta.Height - 5;
            pnlpuerta.Top = pnlpuerta.Top + 5;
        }
    }
}
